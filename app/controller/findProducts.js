/*
 * @Author: LuZeng
 * @Date: 2022-08-16 16:12:09
 * @LastEditTime: 2022-08-22 09:53:34
 * @LastEditors: LuZeng
 * @Description: 小白本白，写的不好多多包涵！！！
 * @FilePath: \jsd:\rjiananzhuang\WEB\WEB workspace\实训三\练习\ThreeNode\app\app\controller\findProducts.js
 * 别乱动！
 */
"use strict";

const Controller = require("egg").Controller;
// 引入响应体
let Response = require("../utils/Response");

class productsService extends Controller {
  // 查询所有商品信息
  async findAllProducts() {
    // 底层egg相关的模块挂载
    const { ctx, service } = this;
    // 调用service层方法
    let products = await service.findProducts.findAll();
    // 接口返回内容
    ctx.body = new Response("查询成功", products);
  }

  // 根据商品分类查询商品信息
  async findByName() {
    let { ctx, service } = this;
    // 获取参数
    let name = ctx.query.name;
    // 调用service层方法
    let products = await service.findProducts.findAll();
    let products_list = [];
    products_list = products.filter((item) => {
      return item.description == name;
    });
    // 给出响应
    ctx.body = new Response("查询成功", products_list);
  }

  //根据商品id查询商品信息
  async findById() {
    let { ctx, service } = this;
    // 获取参数
    let id = ctx.query.id;
    // 调用service层方法
    let product = await service.findProducts.findById(id);
    ctx.body = new Response("查询成功", product);
  }
}

module.exports = productsService;
