'use strict'
const Controller = require('egg').Controller
let Response = require('../utils/Response')

class orderService extends Controller {
  // 信息录入 ---- 新增
  async addStudentInfo() {
    const { ctx, service } = this
    await service.goodsOrder.addStudentInfo(ctx.request.body)
  }

  // 信息录入 ---- 删除
  async deleteStudentInfo() {
    const { ctx, service } = this
    await service.goodsOrder.deleteStudentInfo(ctx.request.body)
  }

  // 信息录入 ---- 编辑
  async editStuInfo() {
    const { ctx, service } = this
    await service.goodsOrder.editStuInfo(ctx.request.body)
  }

  // 报名管理 ---- 新增订单
  async addOrder() {
    const { ctx, service } = this
    await service.goodsOrder.addOrder(ctx.request.body)
  }

  // 报名管理 ---- 分页
  async queryOrderList() {
    const { ctx, service } = this
    await service.goodsOrder.queryOrderList(ctx.query)
  }

  // 报名管理 ---- 申请删除
  async applyDelete() {
    const { ctx, service } = this
    await service.goodsOrder.applyDelete(ctx.request.body)
  }

  // 报名管理 ---- 申请退款
  async applyRefund() {
    const { ctx, service } = this
    await service.goodsOrder.applyRefund(ctx.request.body)
  }

  // 报名管理 ---- 已经退款
  async refund() {
    const { ctx, service } = this
    await service.goodsOrder.refund(ctx.request.body)
  }

  // 审批管理---- 分页数据
  async approveData() {
    const { ctx, service } = this
    await service.goodsOrder.approveData(ctx.request.body)
  }

  // 审批管理 ---- 审批通过
  async approveOrder() {
    const { ctx, service } = this
    await service.goodsOrder.approveOrder(ctx.request.body)
  }

  // 审批管理 ---- 审批驳回
  async rejectOrder() {
    const { ctx, service } = this
    await service.goodsOrder.rejectOrder(ctx.request.body)
  }
}

module.exports = orderService
