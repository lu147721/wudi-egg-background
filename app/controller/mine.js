/*
 * @Author: LuZeng
 * @Date: 2022-08-25 13:34:43
 * @LastEditTime: 2022-08-26 16:48:15
 * @LastEditors: LuZeng
 * @Description: 小白本白，写的不好多多包涵！！！
 * @FilePath: \jsd:\rjiananzhuang\WEB\WEB workspace\实训三\练习\ThreeNode\app\app\controller\mine.js
 * 别乱动！
 */
"use strict";

const Controller = require("egg").Controller;
// 引入响应体
let Response = require("../utils/Response");

class mineService extends Controller {
  // 查询自己信息
  async getOwnData() {
    let { ctx, service } = this;
    // 获取参数
    let username = ctx.query.username;
    // 调用service层方法查询
    let userInfo = await service.mine.getOwnData(username);
    // 给出响应
    ctx.body = new Response("查询成功", userInfo);
  }

  // 更新自己信息
  async updateOwnData() {
    let { ctx, service } = this;
    // 获取参数
    let obj = ctx.query.obj;
    obj = JSON.parse(obj);
    // 调用service层方法。更新数据
    let message = await service.mine.updateOwnData(obj);
    if (message == 1) {
      ctx.body = new Response("更新成功");
    }
  }
}

module.exports = mineService;
