'use strict'
const Controller = require('egg').Controller
let Response = require('../utils/Response')

class mymodelService extends Controller {
  // 我的学生 ----  分页数据
  async queryStudents() {
    const { ctx, service } = this
    await service.mymodel.queryStudents(ctx.request.body)
  }

  // 我的模块 ---- 我的课程下拉
  async myScourse() {
    const { ctx, service } = this
    await service.mymodel.myScourse(ctx.request.body)
  }

  // 我的课表 --- 课程下拉
  async myCourseList() {
    const { ctx, service } = this
    await service.mymodel.myCourseList(ctx.request.body)
  }
}
module.exports = mymodelService
