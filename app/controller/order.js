/*
 * @Author: LuZeng
 * @Date: 2022-08-22 23:19:13
 * @LastEditTime: 2022-08-26 19:27:43
 * @LastEditors: LuZeng
 * @Description: 小白本白，写的不好多多包涵！！！
 * @FilePath: \jsd:\rjiananzhuang\WEB\WEB workspace\实训三\练习\ThreeNode\app\app\controller\order.js
 * 别乱动！
 */
"use strict";

const Controller = require("egg").Controller;
// 引入响应体
let Response = require("../utils/Response");

class orderService extends Controller {
  // 查询所有订单信息
  async getAllOrder() {
    let { ctx, service } = this;
    // 获取参数
    let username = ctx.query.username;
    // 调用service层方法查询
    let orderList = await service.order.findAllOrder(username);
    // 给出响应
    ctx.body = new Response("查询成功", orderList);
  }

  // 查询购物车信息
  async getCartList() {
    let { ctx, service } = this;
    // 获取参数
    let username = ctx.query.username;
    // 调用service层方法查询
    let cartList = await service.order.findCart(username);
    // 给出响应
    ctx.body = new Response("查询成功", cartList);
  }

  //删除购物车商品
  async deleteCartProduct() {
    let { ctx, service } = this;
    // 获取参数
    // 用户名
    let username = ctx.query.username;
    // 商品名
    let proname = ctx.query.proname;
    // 调用service层方法查询
    let massage = await service.order.deleteCartProduct(username, proname);
    // 如果删除更新成功，返回1，向前台返回成功消息
    if (massage == 1) {
      ctx.body = new Response("删除成功");
    }
  }

  // 删除已完成订单
  async deleteCompleted() {
    let { ctx, service } = this;
    // 获取参数
    let username = ctx.query.username;
    let comname = ctx.query.comname;
    // 调用service层方法
    let message = await service.order.deleteCompleted(username, comname);
    // 如果删除更新成功，返回1，向前台返回成功消息
    if (message == 1) {
      ctx.body = new Response("删除成功");
    }
  }

  // 点击确认收货按钮
  async confirm() {
    let { ctx, service } = this;
    // 获取请求参数
    let username = ctx.query.username;
    let obj = ctx.query.confirm;
    // 调用service层方法
    let res = await service.order.confirm(username, obj);
    if (res == 1) {
      ctx.body = new Response("收货成功");
    }
  }

  // 查询已完成信息
  async getCompleted() {
    let { ctx, service } = this;
    // 获取参数
    let username = ctx.query.username;
    // 调用service层方法
    let completedPros = await service.order.getCompleted(username);
    ctx.body = new Response("查询成功", completedPros);
  }
}

module.exports = orderService;
