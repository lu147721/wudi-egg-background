'use strict'
const Controller = require('egg').Controller
let Response = require('../utils/Response')

class organizationService extends Controller {
  // 教师管理 ---- 分页
  async getDataPage() {
    const { ctx, service } = this
    await service.organization.getDataPage(ctx.request.body)
  }

  // 教师管理 ---- 新增
  async addTeacher() {
    const { ctx, service } = this
    await service.organization.addTeacher(ctx.request.body)
  }

  // 教师管理 ---- 删除
  async deleteTeacher() {
    const { ctx, service } = this
    await service.organization.deleteTeacher(ctx.request.body)
  }

  // 教师管理 ---- 编辑
  async editTeacher() {
    const { ctx, service } = this
    await service.organization.editTeacher(ctx.request.body)
  }

  // 教师管理 ---- 重置密码
  async resetPassword() {
    const { ctx, service } = this
    await service.organization.resetPassword(ctx.request.body)
  }

  // 教师管理 ---- 职位下拉
  async roleList() {
    const { ctx, service } = this
    await service.organization.roleList()
  }

  // 职位管理 ---- 分页
  async roleDataPage() {
    const { ctx, service } = this
    await service.organization.roleDataPage(ctx.request.body)
  }

  // 职位管理 ---- 权限页面下拉
  async powerSelect() {
    const { ctx, service } = this
    await service.organization.powerSelect()
  }

  // 职位管理 ---- 新增职位
  async addRole() {
    const { ctx, service } = this
    await service.organization.addRole(ctx.request.body)
  }

  // 职位管理 ---- 删除
  async deleteRole() {
    const { ctx, service } = this
    await service.organization.deleteRole(ctx.request.body)
  }

  // 职位管理 ---- 编辑职位
  async editRole() {
    const { ctx, service } = this
    await service.organization.editRole(ctx.request.body)
  }
}

module.exports = organizationService
