'use strict'
const Controller = require('egg').Controller
let Response = require('../utils/Response')

class publicService extends Controller {
  // 获取课程name与id列表
  async queryCurList() {
    const { ctx, service } = this
    const result = await service.public.queryCurList()
    ctx.body = new Response('查询成功', 200, result)
  }

  // 获取老师信息列表
  async queryTeaList() {
    const { ctx, service } = this
    const result = await service.public.queryTeaList()
    ctx.body = new Response('查询成功', 200, result)
  }

  // 获取周次列表信息
  async queryWeeklyList() {
    const { ctx, service } = this
    const result = await service.public.queryWeeklyList()
    ctx.body = new Response('查询成功', 200, result)
  }

  // 获取教师信息列表
  async queryClassRoomList() {
    const { ctx, service } = this
    const result = await service.public.queryClassRoomList()
    ctx.body = new Response('查询成功', 200, result)
  }

  // 获取每天课次时间列表
  async queryDayTimeList() {
    const { ctx, service } = this
    const result = await service.public.queryDayTimeList()
    ctx.body = new Response('查询成功', 200, result)
  }

  // 获取现在已经排课的课程
  async queryPlanCourses() {
    const { service } = this
    await service.public.queryPlanCourses()
  }

  // 获取所有学生信息
  async queryAllStudent() {
    const { ctx, service } = this
    await service.public.queryAllStudent(ctx.request.body)
  }

  // 学生列表下拉
  async queryStuSelect() {
    const { service } = this
    await service.public.queryStuSelect()
  }

  // 获取动态路由
  async getRouter() {
    const { ctx, service } = this
    await service.public.getRouter(ctx.request.body)
  }

  // 获取当前登录用户信息
  async getUserInfo() {
    const { ctx, service } = this
    await service.public.getUserInfo(ctx.request.body)
  }

  // 更新用户信息
  async updateInfo() {
    const { ctx, service } = this
    await service.public.updateInfo(ctx.request.body)
  }

  // 修改当前登录用户密码
  async editPass() {
    const { ctx, service } = this
    await service.public.editPass(ctx.request.body)
  }

  // 更新登录用户密码
  async updatePass() {
    const { ctx, service } = this
    await service.public.updatePass(ctx.request.body)
  }

  // 学员总数
  async studentNum() {
    const { ctx, service } = this
    await service.public.studentNum()
  }

  // 教师总数
  async teacherNum() {
    const { ctx, service } = this
    await service.public.teacherNum()
  }

  // 课程数
  async courseNum() {
    const { ctx, service } = this
    await service.public.courseNum()
  }

  // 订单数
  async orderNum() {
    const { ctx, service } = this
    await service.public.orderNum()
  }
}

module.exports = publicService
