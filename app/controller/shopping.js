/*
 * @Author: LuZeng
 * @Date: 2022-08-21 16:53:07
 * @LastEditTime: 2022-08-23 19:00:12
 * @LastEditors: LuZeng
 * @Description: 小白本白，写的不好多多包涵！！！
 * @FilePath: \jsd:\rjiananzhuang\WEB\WEB workspace\实训三\练习\ThreeNode\app\app\controller\shopping.js
 * 别乱动！
 */
"use strict";

const Controller = require("egg").Controller;
// 引入响应体
let Response = require("../utils/Response");

class shoppingService extends Controller {
  // 领券购买模块
  // 获取商品信息，后台请求数据，根据id,对比是否存在，如果存在就数量加1后更新保存，如果没有则添加
  async addCommodity() {
    // 挂载底层相关的东西
    let { ctx, service } = this;
    // 获取参数信息，这里的参数是String数据
    let obj = ctx.query.productInfo;
    let userName = ctx.query.userName;
    // 调用service层参数将参数传递下一层
    let order = await service.shopping.findOrder(userName, obj);
    if (order == 1) {
      ctx.body = new Response("购买成功");
    }
  }

  // 加入购物车模块
  // 获取商品信息，后台请求数据，根据id,对比是否存在，如果存在就数量加1后更新保存，如果没有则添加
  async addShoppingCart() {
    // 挂载底层相关东西
    let { ctx, service } = this;
    // 获取参数信息，这里的参数是String数据
    let obj = ctx.query.productInfo;
    let userName = ctx.query.userName;
    // 调用service层方法，将参数传递到service层
    let addCartMassage = await service.shopping.addShoppingCart(userName, obj);
    if (addCartMassage == 1) {
      ctx.body = new Response("加入购车成功");
    }
  }
}

module.exports = shoppingService;
