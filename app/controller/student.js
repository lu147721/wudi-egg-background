// controller/students.js
'use strict'

const Controller = require('egg').Controller

// 引入响应体
let Response = require('../utils/Response')

class StudentController extends Controller {
  // vscode 有默认的用户片段 输入controller即可生成
  async findAll() {
    // egg规定方法都是异步的
    // 应该是底层把egg相关的东西都挂上去了
    const { ctx, service } = this
    // 调用service层查询方法
    let students = await service.student.findAll()
    // 接口返回内容
    ctx.body = new Response('查询成功', students)
  }

  // 根据id查询学生信息
  async findById() {
    let { ctx, service } = this
    // 1、获取参数
    let id = ctx.query.id
    // 调用service层方法
    // await service.findById(id);
    let student = await service.student.findById(id)
    // 给出响应
    ctx.body = new Response('查询成功', student)
  }

  async lists() {
    const { ctx } = this

    await new Promise((resolve) => {
      // 模拟接口延迟
      setTimeout(() => {
        resolve()
      }, 1500)
    })

    ctx.body = [{ id: 123 }]
  }
}

module.exports = StudentController
