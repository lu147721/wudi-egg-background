'use strict'

const Controller = require('egg').Controller

// 引入响应体
let Response = require('../utils/Response')

class teachingService extends Controller {
  // 课程管理----分页
  async findAllCourse() {
    let { ctx, service } = this
    // 获取参数
    const couerseInfo = ctx.query
    const result = await service.teaching.findAllCourse(couerseInfo)
    ctx.body = new Response('查询成功', 200, result)
  }

  // 课程管理----新增
  async addCourseInfo() {
    let { ctx, service } = this
    const courseInfo = ctx.request.body
    const result = await service.teaching.addCourseInfo(courseInfo)
    if (result === 1) {
      throw new Response('课程编码重复')
    } else if (result === 2) {
      throw new Response('课程名称重复')
    } else if (result === 3) {
      throw new Response('添加失败')
    } else {
      ctx.body = new Response('添加成功', 200)
    }
  }

  // 课程管理---编辑
  async editCourse() {
    const { ctx, service } = this
    const courseInfo = ctx.request.body
    const result = await service.teaching.editCourse(courseInfo)
    if (result) {
      ctx.body = new Response('编辑成功', 200)
    } else {
      throw new Response('编辑失败')
    }
  }

  // 课程管理----删除
  async deleteCourse() {
    const { ctx, service } = this
    const { code } = ctx.request.body
    const result = await service.teaching.deleteCourse(code)
    if (result) {
      ctx.body = new Response('删除成功', 200)
    } else {
      throw new Response('删除失败')
    }
  }

  // 教室管理----分页
  async getClassAllRoom() {
    const { ctx, service } = this
    const result = await service.teaching.getClassAllRoom(ctx.query)
    ctx.body = new Response('查询成功', 200, result)
  }

  // 教室管理----新增
  async addClassRoom() {
    const { ctx, service } = this
    const result = await service.teaching.addClassRoom(ctx.request.body)
    if (result) {
      ctx.body = new Response('添加成功', 200)
    } else {
      throw new Response('添加失败')
    }
  }

  // 教室管理----编辑
  async editClassRoom() {
    const { ctx, service } = this
    const result = await service.teaching.editClassRoom(ctx.request.body)
    if (result) {
      ctx.body = new Response('编辑成功', 200)
    } else {
      throw new Response('编辑失败')
    }
  }

  // 教室管理---删除
  async deleteClassRoom() {
    const { ctx, service } = this
    const result = await service.teaching.deleteClassRoom(ctx.request.body)
    if (result) {
      ctx.body = new Response('删除成功', 200)
    } else {
      throw new Response('删除失败')
    }
  }

  // 排课计划 ---- 新增
  async addCourse() {
    const { ctx, service } = this
    const result = await service.teaching.addCourse(ctx.request.body)
    ctx.body = new Response('操作成功', 200, result)
  }

  // 排课计划 ---- 分页模糊查询
  async pageClassPlanList() {
    const { ctx, service } = this
    const result = await service.teaching.pageClassPlanList(ctx.query)
    try {
      ctx.body = new Response('查询成功', 200, result)
    } catch (err) {
      throw new Response('查询失败', 500)
    }
  }

  // 排课计划 ---- 删除
  async deleteClassPlanList() {
    const { ctx, service } = this
    await service.teaching.deleteClassPlanList(ctx.request.body)
  }

  // 排课计划 ---- 编辑
  async updateClassPlan() {
    const { ctx, service } = this
    await service.teaching.updateClassPlan(ctx.request.body)
  }

  // 课程表 ---- 列表数据
  async queryClassTime() {
    const { ctx, service } = this
    await service.teaching.queryClassTime(ctx.request.body)
  }

  // 课程表 ---- 已经排课的教师下拉
  async queryTeacherList() {
    const { service } = this
    await service.teaching.queryTeacherList()
  }
}

module.exports = teachingService
