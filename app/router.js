// app/router.js
'use strict'

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = (app) => {
  const { router, controller, jwt } = app

  router.get('/', controller.home.index)
  // 登录
  router.post('/login', controller.logon.logon)
  /**
   * 公共接口
   */
  // 获取全部课程列表
  router.get('/public/queryCurList', jwt, controller.public.queryCurList)
  // 获取教师信息列表
  router.get('/public/queryTeaList', jwt, controller.public.queryTeaList)
  // 获取周次列表信息
  router.get('/public/queryWeeklyList', jwt, controller.public.queryWeeklyList)
  // 获取教室信息列表
  router.get('/public/queryClassRoomList', jwt, controller.public.queryClassRoomList)
  // 获取每天课次时间列表
  router.get('/public/queryDayTimeList', jwt, controller.public.queryDayTimeList)
  // 获取现在已经排课的课程
  router.get('/public/queryPlanCourses', jwt, controller.public.queryPlanCourses)
  // 获取全部学生下拉列表
  router.get('/public/queryStuSelect', jwt, controller.public.queryStuSelect)
  // 获取路由
  router.post('/public/getRouter', jwt, controller.public.getRouter)
  // 课程表 ---- 列表数据
  router.post('/teaching/coursePlan/queryClassTime', jwt, controller.teaching.queryClassTime)
  // 个人中心 ---- 获取当前登录用户信息
  router.post('/public/getUserInfo', jwt, controller.public.getUserInfo)
  // 个人中心 ---- 更新当前登录用户信息
  router.post('/public/updateInfo', jwt, controller.public.updateInfo)
  // 个人中心 ---- 修改密码
  router.post('/public/editPass', jwt, controller.public.editPass)
  // 首页 ---- 学员总数
  router.get('/public/studentNum', jwt, controller.public.studentNum)
  // 首页 ---- 教师总数
  router.get('/public/teacherNum', jwt, controller.public.teacherNum)
  // 首页 ---- 课程总数
  router.get('/public/courseNum', jwt, controller.public.courseNum)
  // 首页 ---- 订单总数
  router.get('/public/orderNum', jwt, controller.public.orderNum)

  /**
   * 教务模块
   */
  // 教务管理->课程管理->分页课程信息
  router.get('/teaching/courseInfo/page', jwt, controller.teaching.findAllCourse)
  // 课程管理----新增
  router.post('/teaching/courseInfo/add', jwt, controller.teaching.addCourseInfo)
  // 课程管理---编辑
  router.post('/teaching/courseInfo/edit', jwt, controller.teaching.editCourse)
  // 课程管理---删除
  router.post('/teaching/courseInfo/delete', jwt, controller.teaching.deleteCourse)
  // 教室管理 ---- 新增
  router.get('/teaching/classRoom/page', jwt, controller.teaching.getClassAllRoom)
  // 教室管理 ---- 编辑
  router.post('/teaching/classRoom/edit', jwt, controller.teaching.editClassRoom)
  // 教室管理 ---- 新增
  router.post('/teaching/classRoom/add', jwt, controller.teaching.addClassRoom)
  // 教室管理 ---- 删除
  router.post('/teaching/classRoom/delete', jwt, controller.teaching.deleteClassRoom)
  // 排课计划 --- 新增
  router.post('/teaching/coursePlan/addCourse', jwt, controller.teaching.addCourse)
  // 排课计划 ---- 分页查询
  router.get('/teaching/coursePlan/pageClassPlanList', jwt, controller.teaching.pageClassPlanList)
  // 排课计划 ---- 删除
  router.post('/teaching/coursePlan/deleteClassPlanList', jwt, controller.teaching.deleteClassPlanList)
  // 排课计划 ---- 编辑
  router.post('/teaching/coursePlan/updateClassPlan', jwt, controller.teaching.updateClassPlan)
  // 课程表 ---- 查询已经排课的教师
  router.get('/teaching/queryTeacherList', jwt, controller.teaching.queryTeacherList)
  /**
   * 运营管理模块
   */
  // 信息录入 --- 分页
  router.post('/teaching/goodsOrder/allStudent', jwt, controller.public.queryAllStudent)
  // 信息录入 ---- 新增
  router.post('/teaching/goodsOrder/addStudentInfo', jwt, controller.goodsOrder.addStudentInfo)
  // 信息录入 ---- 删除
  router.post('/teaching/goodsOrder/deleteStudentInfo', jwt, controller.goodsOrder.deleteStudentInfo)
  // 信息录入 ---- 编辑
  router.post('/teaching/goodsOrder/editStuInfo', jwt, controller.goodsOrder.editStuInfo)
  // 报名管理 ---- 新增订单
  router.post('/teaching/goodsOrder/addOrder', jwt, controller.goodsOrder.addOrder)
  // 报名管理 ---- 分页
  router.get('/teaching/goodsOrder/page', jwt, controller.goodsOrder.queryOrderList)
  // 报名管理 ---- 申请删除
  router.post('/teaching/goodsOrder/applyDelete', jwt, controller.goodsOrder.applyDelete)
  // 报名管理 ---- 申请退款
  router.post('/teaching/goodsOrder/applyRefund', jwt, controller.goodsOrder.applyRefund)
  // 报名管理 ---- 已经退款
  router.post('/teaching/goodsOrder/refund', jwt, controller.goodsOrder.refund)
  // 审批模块 ---- 分页数据
  router.post('/goodsOrder/page', jwt, controller.goodsOrder.approveData)
  // 审批管理 ---- 审批通过
  router.post('/goodsOrder/approve', jwt, controller.goodsOrder.approveOrder)
  // 审批管理 ---- 审批驳回
  router.post('/goodsOrder/reject', jwt, controller.goodsOrder.rejectOrder)

  /**
   * 人员职位管理模块
   */
  // 教师页面 ---- 分页数据
  router.post('/organization/page', jwt, controller.organization.getDataPage)
  // 教师页面 ---- 新增教师
  router.post('/organization/addTeacher', jwt, controller.organization.addTeacher)
  // 教师页面 ---- 删除教师
  router.post('/organization/deleteTeacher', jwt, controller.organization.deleteTeacher)
  // 教师页面 ---- 修改信息
  router.post('/organization/editTeacher', jwt, controller.organization.editTeacher)
  // 教师页面 ---- 重置密码
  router.post('/organization/resetPassword', jwt, controller.organization.resetPassword)
  // 教师页面 ---- 职位列表下拉
  router.get('/organization/roleList', jwt, controller.organization.roleList)
  // 职位管理 ---- 分页
  router.post('/organization/roleDataPage', jwt, controller.organization.roleDataPage)
  // 职位管理 ---- 权限页面下拉
  router.post('/organization/powerSelect', jwt, controller.organization.powerSelect)
  // 职位管理 ---- 新增职位
  router.post('/organization/addRole', jwt, controller.organization.addRole)
  // 职位管理 ---- 删除职位
  router.post('/organization/deleteRole', jwt, controller.organization.deleteRole)
  // 职位管理 ---- 编辑职位
  router.post('/organization/editRole', jwt, controller.organization.editRole)
  /**
   * 我的模块
   */
  // 我的学生 ---- 课程下拉
  router.post('/mymodel/myScourse', jwt, controller.mymodel.myScourse)
  // 我的学生 ---- 分页数据
  router.post('/mymodel/queryStudents', jwt, controller.mymodel.queryStudents)
  // 我的课表 ---- 课程下拉
  router.post('/mymodel/myCourseList', jwt, controller.mymodel.myCourseList)
  /**
   * Moon 模块，此次项目不需要用
   */
  // 1查询所有学生信息
  router.get('/student', jwt, controller.student.findAll) // 新增
  // 2根据id查询学生信息;
  router.get('/student/findById', jwt, controller.student.findById)
  // 3查询所有商品信息
  router.get('/products/findAll', jwt, controller.findProducts.findAllProducts)
  // 4根据商品分类查询商品信息
  router.get('/products/findName', jwt, controller.findProducts.findByName)
  // 5根据商品id查询商品信息
  router.get('/products/findId', jwt, controller.findProducts.findById)
  // 6更新购物车数据信息
  router.get('/shopping/update', jwt, controller.shopping.addCommodity)
  // 7查询所有订单信息
  router.get('/order/findAll', jwt, controller.order.getAllOrder)
  // 8添加购物车
  router.get('/shopping/addCart', jwt, controller.shopping.addShoppingCart)
  // 9查询购物车信息
  router.get('/order/findCart', jwt, controller.order.getCartList)
  // 10删除购物车商品
  router.get('/order/delete', jwt, controller.order.deleteCartProduct)
  // 11查询个人信息
  router.get('/mine/findOwn', jwt, controller.mine.getOwnData)
  // 12确认收货
  router.get('/order/confirm', jwt, controller.order.confirm)
  // 13查询已完成订单
  router.get('/order/completed', jwt, controller.order.getCompleted)
  // 14更新自己更改后的信息
  router.get('/mine/updateOwnData', jwt, controller.mine.updateOwnData)
  // 15注册新用户
  router.post('/user/register', controller.logon.register)
  // 16删除订单信息
  router.get('/order/deleteCompleted', jwt, controller.order.deleteCompleted)
  router.get('/student/lists', controller.student.lists) // 新增
}
