/*
 * @Author: LuZeng
 * @Date: 2022-08-21 10:33:03
 * @LastEditTime: 2022-08-21 11:04:07
 * @LastEditors: LuZeng
 * @Description: 小白本白，写的不好多多包涵！！！
 * @FilePath: \jsd:\rjiananzhuang\WEB\WEB workspace\实训三\练习\ThreeNode\app\app\service\findProducts.js
 * 别乱动！
 */
/*
 * @Author: LuZeng
 * @Date: 2022-08-16 16:12:38
 * @LastEditTime: 2022-08-21 10:30:25
 * @LastEditors: LuZeng
 * @Description: 小白本白，写的不好多多包涵！！！
 * @FilePath: \jsd:\rjiananzhuang\WEB\WEB workspace\实训三\练习\ThreeNode\app\app\service\findProducts.js
 * 别乱动！
 */
const { Service } = require("egg");
class productsService extends Service {
  // 查询所有商品信息
  async findAll() {
    let { mysql } = this.app;
    const products = await mysql.select("products");
    return products;
  }

  // 根据name查询商品信息
  async findByName(name) {
    let { mysql } = this.app;
    const products = await mysql.select("products", { description: name });
    return products;
  }

  // 根据id查询商品信息
  async findById(id) {
    let { mysql } = this.app;
    const product = await mysql.get("products", { id });
    return product;
  }
}
module.exports = productsService;
