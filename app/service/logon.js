const { Service } = require('egg')

const Response = require('../utils/Response')

class UserLogon extends Service {
  async logon({ account, password }) {
    const { mysql } = this.app

    // 查询用户信息（根据用户名）
    const [accountInfo] = await mysql.query(
      'SELECT tea_password, tea_name, role, tea_code FROM teachers WHERE email=?',
      [account]
    )
    // 不为空时，如果密码相等就返回用户信息，否则密码错误
    if (accountInfo?.tea_password) {
      if (accountInfo.tea_password !== password) {
        throw new Response('密码错误')
      }
      delete accountInfo.tea_password
      return accountInfo
    }
    throw new Response('该用户不存在')
  }

  // 注册新用户
  async register(userInfo) {
    const { mysql } = this.app
    // 先检出用没有这个用户名，有的话停止注册，并返回已经存在信息
    // 如果不存在，就插入信息，并更新购物车等一些列数据
    const res = await mysql.get('logon', {
      username: userInfo.username,
    })
    if (res == null) {
      const arr = '[]'
      // 插入
      const result = await this.app.mysql.insert('logon', {
        username: userInfo.username,
        password: userInfo.password,
        cart: arr,
        shoppingcart: arr,
        completed: arr,
        receiving: arr,
      })
      // 判断插入成功
      return result.affectedRows
    }
    // 失败返回-1
    return -1
  }
}
module.exports = UserLogon
