/*
 * @Author: LuZeng
 * @Date: 2022-08-25 13:37:42
 * @LastEditTime: 2022-08-26 16:48:42
 * @LastEditors: LuZeng
 * @Description: 小白本白，写的不好多多包涵！！！
 * @FilePath: \jsd:\rjiananzhuang\WEB\WEB workspace\实训三\练习\ThreeNode\app\app\service\mine.js
 * 别乱动！
 */
const { Service } = require("egg");
class mineService extends Service {
  // 查询自己信息
  async getOwnData(username) {
    let { mysql } = this.app;
    let userInfo = await mysql.get("logon", { username });
    return userInfo;
  }

  // 更新自己更改后的数据
  async updateOwnData(obj) {
    let { mysql } = this.app;

    // 如果主键是自定义的 ID 名称，如 custom_id，则需要在 `where` 里面配置
    const row = {
      fullname: obj.fullname,
      hobby: obj.hobby,
      brith: obj.brith,
      email: obj.email,
      autograph: obj.autograph,
      industry: obj.industry,
    };

    const options = {
      where: {
        username: obj.name,
      },
    };
    const result = await mysql.update("logon", row, options); // 更新 posts 表中的记录

    // 判断更新成功
    return result.affectedRows;
  }
}
module.exports = mineService;
