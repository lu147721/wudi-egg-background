/*
 * @Author: LuZeng
 * @Date: 2022-08-22 23:19:25
 * @LastEditTime: 2022-08-26 19:33:41
 * @LastEditors: LuZeng
 * @Description: 小白本白，写的不好多多包涵！！！
 * @FilePath: \jsd:\rjiananzhuang\WEB\WEB workspace\实训三\练习\ThreeNode\app\app\service\order.js
 * 别乱动！
 */
const { Service } = require("egg");
class orderService extends Service {
  // 查询所有订单信息
  async findAllOrder(username) {
    let { mysql } = this.app;
    let userInfo = await mysql.get("logon", { username });
    let order = userInfo.cart;
    return order;
  }

  // 查询购物车信息
  async findCart(username) {
    let { mysql } = this.app;
    let userInfo = await mysql.get("logon", { username });
    let order = userInfo.shoppingcart;
    return order;
  }

  // 删除购物车商品信息
  async deleteCartProduct(username, proname) {
    // 思路：1、查到用户数据 2、拿到cart的商品信息 3、将json字符串转化成对象格式
    //       4、findIndex找到对应name商品的index 5、删除数组对象的哪一项，然后将剩下的数据转化为JSON字符串更新到数据库
    //       6、在次代用查询方法将页面的数据重新刷新一下
    let { mysql } = this.app;
    let userInfo = await mysql.get("logon", { username });
    // 拿到cart并且转化数据类型
    let cartList = JSON.parse(userInfo.shoppingcart);
    // 拿到对应项的ID
    let indexPro = cartList.findIndex((item) => {
      return item.name == proname;
    });
    // 删除对应商品
    cartList.splice(indexPro, 1);
    // 这样arr里面接收的是被删除掉的项
    // let arr = cartList.splice(indexPro, 1);
    // 转化为JSON字符串
    cartList = JSON.stringify(cartList);
    // 更新到数据库
    const row = {
      shoppingcart: cartList,
    };
    const options = {
      where: {
        username,
      },
    };
    const result = await this.app.mysql.update("logon", row, options); // 更新 posts 表中的记录

    // 判断更新成功
    return result.affectedRows;
  }

  // 删除完成订单信息
  async deleteCompleted(username, comname) {
    let { mysql } = this.app;
    let userInfo = await mysql.get("logon", { username });
    // 拿到cart并且转化数据类型
    let comList = JSON.parse(userInfo.completed);
    // 拿到对应项的ID
    let indexCom = comList.findIndex((item) => {
      return item.name == comname;
    });
    // 删除对应订单
    comList.splice(indexCom, 1);
    // 这样arr里面接收的是被删除掉的项
    // let arr = cartList.splice(indexPro, 1);
    // 转化为JSON字符串
    comList = JSON.stringify(comList);
    // 更新到数据库
    const row = {
      completed: comList,
    };
    const options = {
      where: {
        username,
      },
    };
    const result = await this.app.mysql.update("logon", row, options); // 更新 posts 表中的记录

    // 判断更新成功
    return result.affectedRows;
  }

  // 点击确认收货按钮
  async confirm(username, obj) {
    // 思路：
    // 拿到自己未收货和完成里面的收据，数据库与参数数据转化，
    // 未收货找到对应数据删除，完成存入传进的参数
    let { mysql } = this.app;
    let userInfo = await mysql.get("logon", { username });
    let cart = JSON.parse(userInfo.cart);
    obj = JSON.parse(obj);
    let completed;
    // 进行判断completed里面有无数据
    // 有的话数据转化，没有就初始化为数组
    if (userInfo.completed == " " || userInfo.completed == null) {
      completed = [];
    } else {
      completed = JSON.parse(userInfo.completed);
    }
    // 查看cart中索引
    let proIndex = cart.findIndex((item) => {
      return item.name == obj.name;
    });
    // 删除
    cart.splice(proIndex, 1);
    // 插入
    completed.push(obj);

    // 更新数据
    // 如果主键是自定义的 ID 名称，如 custom_id，则需要在 `where` 里面配置
    const row = {
      cart: JSON.stringify(cart),
      completed: JSON.stringify(completed),
    };

    const options = {
      where: {
        username,
      },
    };
    const result = await this.app.mysql.update("logon", row, options); // 更新 posts 表中的记录

    // 判断更新成功
    return result.affectedRows;
  }

  // 查询已完成订单
  async getCompleted(username) {
    let { mysql } = this.app;
    // 查询数据库
    let userInfo = await mysql.get("logon", { username });
    // 返回已完成订单信息
    let data = userInfo.completed;
    return data;
  }
}
module.exports = orderService;
