const { Service } = require('egg')
const Response = require('../utils/Response')

class organizationService extends Service {
  // 教师管理 ---- 分页
  async getDataPage(params) {
    const { ctx } = this
    const { mysql } = this.app
    const offSet = params.page === '1' ? 0 : (Number(params.page) - 1) * Number(params.pageSize)
    const pageSize = Number(params.pageSize)
    const tea_name = params?.name ? params.name : null
    const sqlTotal = `SELECT COUNT(*) as cnt FROM teachers 
      WHERE (tea_name LIKE CONCAT('%', ?, '%') OR ? IS NULL)`
    const sql = `SELECT * FROM teachers 
      WHERE (tea_name LIKE CONCAT('%', ?, '%') OR ? IS NULL) 
      LIMIT ? OFFSET ?`
    try {
      const total = await mysql.query(sqlTotal, [tea_name, tea_name, pageSize, offSet])
      const result = await mysql.query(sql, [tea_name, tea_name, pageSize, offSet])
      ctx.body = new Response('查询成功', 200, { total: total[0].cnt, result })
    } catch (err) {
      throw new Response('异常错误', 500)
    }
  }

  // 教师管理 ---- 新增
  async addTeacher(params) {
    const { ctx } = this
    const { mysql } = this.app
    const sql = `INSERT INTO teachers(tea_name, email, tea_password, roleName, role, gender, age, tel) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`
    const password = '123456'
    const { name, age, gender, email, role, tel, roleName } = params
    try {
      const result = await mysql.query(sql, [name, email, password, roleName, role, gender, Number(age), tel])
      ctx.body =
        result.affectedRows === 1
          ? new Response(`创建成功，初始密码为：${password}`, 200)
          : new Response('创建失败', 500)
    } catch (err) {
      throw new Response('异常错误', 500)
    }
  }

  // 教师管理 ---- 删除
  async deleteTeacher(params) {
    const { ctx } = this
    const { mysql } = this.app
    const sql = `DELETE FROM teachers WHERE tea_code=?`
    try {
      const result = await mysql.query(sql, [params.tea_code])
      ctx.body = result.affectedRows === 1 ? new Response('删除成功', 200) : new Response('删除失败', 500)
    } catch (err) {
      throw new Response('异常错误', 500, err)
    }
  }

  // 教师管理 ---- 编辑
  async editTeacher(params) {
    const { ctx } = this
    const { mysql } = this.app
    console.log(params, '????')
    const { tea_code, age, gender, email, role, tel, roleName } = params
    const sql = `UPDATE teachers SET email=?, role=?, roleName=?, gender=?, age=?, tel=? WHERE tea_code=?`
    try {
      const result = await mysql.query(sql, [email, role, roleName, gender, age, tel, tea_code])
      ctx.body = result.affectedRows === 1 ? new Response('编辑成功', 200) : new Response('编辑失败', 500)
    } catch (err) {
      throw new Response('异常错误', 500, err)
    }
  }

  // 教师管理 ---- 重置密码
  async resetPassword(params) {
    const { ctx } = this
    const { mysql } = this.app
    const sql = `UPDATE teachers SET tea_password=? WHERE tea_code=?`
    const pass = '123456'
    try {
      const result = await mysql.query(sql, [pass, params.tea_code])
      ctx.body = result.affectedRows === 1 ? new Response('重置成功', 200, pass) : new Response('重置失败', 200)
    } catch (err) {
      throw new Response('异常错误', 500)
    }
  }

  // 教师管理 ---- 职位下拉
  async roleList() {
    const { ctx } = this
    const { mysql } = this.app
    const sql = `SELECT id, name as roleName FROM roles WHERE statusCode=1`
    try {
      const roleList = await mysql.query(sql)
      ctx.body = new Response('查询成功', 200, roleList)
    } catch (err) {
      throw new Response('异常错误', 500)
    }
  }

  // 职位管理 ---- 分页
  async roleDataPage(params) {
    const { ctx } = this
    const { mysql } = this.app
    const offSet = params.page === '1' ? 0 : (Number(params.page) - 1) * Number(params.pageSize)
    const pageSize = Number(params.pageSize)
    const name = params?.name ? params.name : null
    const sqlTotal = `SELECT COUNT(*) as cnt FROM roles 
      WHERE (name LIKE CONCAT('%', ?, '%') OR ? IS NULL)`
    const sql = `SELECT * FROM roles 
      WHERE (name LIKE CONCAT('%', ?, '%') OR ? IS NULL)
      LIMIT ? OFFSET ?`
    try {
      const total = await mysql.query(sqlTotal, [name, name, pageSize, offSet])
      const result = await mysql.query(sql, [name, name, pageSize, offSet])
      ctx.body = new Response('查询成功', 200, { total: total[0].cnt, result })
    } catch (err) {
      throw new Response('异常错误', 500)
    }
  }

  // 职位管理 ---- 权限页面下拉
  async powerSelect() {
    const { ctx } = this
    const { mysql } = this.app
    const sql = `SELECT * FROM power`
    try {
      const result = await mysql.query(sql)
      ctx.body = new Response('查询成功', 200, result)
    } catch (err) {
      throw new Response('异常错误', 500)
    }
  }

  // 职位管理 ---- 新增职位
  async addRole(params) {
    const { ctx } = this
    const { mysql } = this.app
    const statusName = params.statusCode ? '启用' : '禁用'
    const sql = `INSERT INTO roles(name, statusCode, statusName,roles) VALUES (?, ?, ?, ?)`
    try {
      const result = await mysql.query(sql, [params.name, params.statusCode, statusName, JSON.stringify(params.roles)])
      ctx.body = result.affectedRows === 1 ? new Response('创建成功', 200) : new Response('创建失败', 500)
    } catch (err) {
      throw new Response('异常错误', 500)
    }
  }

  // 职位管理 ---- 删除
  async deleteRole(params) {
    const { ctx } = this
    const { mysql } = this.app
    const sql = `DELETE FROM roles WHERE id=?`
    try {
      const result = await mysql.query(sql, [params.id])
      ctx.body = result.affectedRows === 1 ? new Response('删除成功', 200) : new Response('删除失败', 500)
    } catch (err) {
      throw new Response('异常错误', 500)
    }
  }

  // 职位管理 ---- 编辑职位
  async editRole(params) {
    const { ctx } = this
    const { mysql } = this.app
    const statusName = params.statusCode ? '启用' : '禁用'
    const sql = `UPDATE roles SET statusCode=?, statusName=?,roles=? WHERE id=?`
    try {
      const result = await mysql.query(sql, [params.statusCode, statusName, JSON.stringify(params.roles), params.id])
      ctx.body = result.affectedRows === 1 ? new Response('编辑成功', 200) : new Response('编辑失败', 500)
    } catch (err) {
      throw new Response('异常错误', 500)
    }
  }
}

module.exports = organizationService
