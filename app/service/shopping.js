/*
 * @Author: LuZeng
 * @Date: 2022-08-21 17:04:58
 * @LastEditTime: 2022-08-23 19:03:16
 * @LastEditors: LuZeng
 * @Description: 小白本白，写的不好多多包涵！！！
 * @FilePath: \jsd:\rjiananzhuang\WEB\WEB workspace\实训三\练习\ThreeNode\app\app\service\shopping.js
 * 别乱动！
 */
const { Service } = require("egg");

class shoppingService extends Service {
  // 获取数据，和参数比对，有就num加1，没有就插入
  async findOrder(userName, obj) {
    let { mysql } = this.app;
    // 用户信息
    let userInfo = await mysql.get("logon", { username: userName });
    // 讲obj从字符串转化为obj类型,以备调用
    obj = JSON.parse(obj);
    let objName = obj.name;
    // 将购物车内容给拿出来
    let cartList;
    // 判断购物者是否为空,初始化购物车为一个数组
    // 否则不为空进行类型转换，转化成数组对象类型
    if (userInfo.cart == null || userInfo.cart == " ") {
      cartList = [];
    } else {
      // 转化
      cartList = JSON.parse(userInfo.cart);
    }

    // 拿到当前商品在cart中索引
    let saveNot = cartList.findIndex((item) => {
      return item.name == objName;
    });

    // 如果不存在添加进数组
    // 如果存在就令其数量自加一
    if (saveNot == -1) {
      cartList.push(obj);
    } else {
      cartList[saveNot].num++;
    }

    // 更新到数据库
    // 如果主键是自定义的 ID 名称，如 custom_id，则需要在 `where` 里面配置
    const row = {
      cart: JSON.stringify(cartList),
    };
    const options = {
      where: {
        username: userName,
      },
    };
    const result = await this.app.mysql.update("logon", row, options); // 更新logon 表中的记录
    return result.affectedRows;
  }

  // 添加购物车
  async addShoppingCart(userName, obj) {
    let { mysql } = this.app;
    // 用户信息
    let userInfo = await mysql.get("logon", { username: userName });
    // 将obj从String转化为Object有，以备调用
    obj = JSON.parse(obj);
    let objName = obj.name;
    // 将购物车内容拿出来
    let cartList;
    // 购物车如果为空，初始化为一个空数组
    // 如果不为空，转化成数组对象类型
    if (userInfo.shoppingcart == null || userInfo.shoppingcart == " ") {
      cartList = [];
    } else {
      cartList = JSON.parse(userInfo.shoppingcart);
    }

    // 拿到当前商品在数组中的索引
    let saveNot = cartList.findIndex((item) => {
      return item.name == objName;
    });

    // 不存在添加进数组
    // 存在令其数量加1
    if (saveNot == -1) {
      cartList.push(obj);
    } else {
      cartList[saveNot].num++;
    }

    // 更新到数据库
    const row = {
      shoppingcart: JSON.stringify(cartList),
    };
    const options = {
      where: {
        username: userName,
      },
    };
    const result = await this.app.mysql.update("logon", row, options);
    return result.affectedRows;
  }
}

module.exports = shoppingService;
