/*
 * @Author: LuZeng
 * @Date: 2022-07-29 17:25:14
 * @LastEditTime: 2022-08-21 17:05:49
 * @LastEditors: LuZeng
 * @Description: 小白本白，写的不好多多包涵！！！
 * @FilePath: \jsd:\rjiananzhuang\WEB\WEB workspace\实训三\练习\ThreeNode\app\app\service\student.js
 * 别乱动！
 */
// service/student.js
const { Service } = require('egg')

class studentService extends Service {
  // 查询所有学生信息
  async findAll() {
    let { mysql } = this.app
    const students = await mysql.select('student_information')
    return students
  }

  // 根据id查询学生数据
  async findById(id) {
    let { mysql } = this.app
    const student = await mysql.get('student_information', { student_id: id })
    return student
  }

  // 更新插入学生数据
  async saveOrUpdate(student) {
    let result = null
    if (student.id) {
      result = await this.app.mysql.update('student_information', student)
    } else {
      result = await this.app.mysql.insert('student_information', student)
    }
    return result
  }

  // 根据id删除学生数据
  async deleteById(id) {
    const result = await this.app.mysql.delete('student_information', { id })
  }
}

module.exports = studentService
