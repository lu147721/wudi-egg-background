// 封装响应体
// utils/Response.js

class Response {
  constructor(message, status, data) {
    this.message = message;
    this.data = data;
    this.status = status;
    this.timestamp = new Date().getTime();
  }
}

module.exports = Response;
