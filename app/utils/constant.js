/**
 * 1.首页 2.我的模块 3.教务管理 4.运营管理 5.教师职位管理
 */
const permissionAll = [
  {
    component: 'home/home.vue',
    path: 'home',
    name: 'home',
    meta: {
      name: '首页',
      icon: 'el-icon-s-home',
    },
  },
  {
    component: 'financial-management/index',
    path: 'financial-management',
    name: 'router',
    meta: {
      name: '我的',
      icon: 'el-icon-guide',
    },
    children: [
      {
        component: 'financial-management/tuition-management',
        path: 'tuition-management',
        name: 'TuitionManagement',
        meta: {
          name: '我的课表',
          icon: 'el-icon-set-up',
        },
      },
      {
        component: 'financial-management/salary-management',
        path: 'salary-management',
        name: 'SalaryManagement',
        meta: {
          name: '我的学员',
          icon: 'el-icon-s-operation',
        },
      },
    ],
  },
  {
    component: 'language/index',
    path: 'language',
    name: 'language',
    meta: {
      name: '教务管理',
      icon: 'el-icon-s-flag',
    },
    children: [
      {
        component: 'language/lessons',
        path: 'lessons',
        name: 'lessons',
        meta: {
          name: '课程表',
          icon: 'el-icon-set-up',
        },
      },
      {
        component: 'language/schedule',
        path: 'schedule',
        name: 'schedule',
        meta: {
          name: '排课计划',
          icon: 'el-icon-question',
        },
      },
      {
        component: 'language/curriculum-management',
        path: 'online-trainees',
        name: 'onlineTrainees',
        meta: {
          name: '课程管理',
          icon: 'el-icon-full-screen',
        },
      },
      {
        component: 'language/class-room',
        path: 'class-room',
        name: 'classRoom',
        meta: {
          name: '教室管理',
          icon: 'el-icon-map-location',
        },
      },
    ],
  },
  {
    path: 'icon',
    component: 'icon/index',
    name: 'icon',
    meta: {
      name: '运营管理',
      icon: 'el-icon-s-data',
    },
    children: [
      {
        path: 'intended-customers',
        component: 'icon/intended-customers',
        name: 'intended-customers',
        meta: {
          name: '信息录入',
          icon: 'el-icon-document',
        },
      },
      {
        path: 'elicon',
        component: 'icon/el-icon',
        name: 'elicon',
        meta: {
          name: '报名管理',
          icon: 'el-icon-caret-bottom',
        },
      },
      {
        path: 'aliicon',
        component: 'icon/ali-icon',
        name: 'aliicon',
        meta: {
          name: '审批管理',
          icon: 'el-icon-caret-top',
        },
      },
    ],
  },
  {
    path: 'table',
    component: 'table/index',
    name: 'table',
    meta: {
      name: '职位/人员管理',
      icon: 'el-icon-s-grid',
    },
    children: [
      {
        path: 'basics',
        component: 'table/basics',
        name: 'basics',
        meta: {
          name: '教师管理',
          icon: 'el-icon-menu',
        },
      },
      {
        path: 'complex',
        component: 'table/complex',
        name: 'complex',
        meta: {
          name: '职位管理',
          icon: 'el-icon-s-grid',
        },
      },
    ],
  },
]

module.exports = permissionAll
