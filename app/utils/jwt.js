module.exports = (options) => {
  return async function jwt(ctx, next) {
    const token = ctx.request.header.authorization
    let decode
    if (token) {
      try {
        // 解码token
        decode = ctx.app.jwt.verify(token, options.secret)
        await next()
      } catch (error) {
        ctx.body = {
          code: 403,
          message: error.message,
        }
        return
      }
    } else {
      ctx.body = {
        code: 403,
        message: '没有token',
      }
      return
    }
  }
}
