'use strict'

const { cors } = require('./plugin')

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = (appInfo) => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = (exports = {})

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1658564004302_6211'

  // add your user config here
  const userConfig = {
    jwt: {
      secret: 'zzysb',
    },
    // myAppName: 'egg',
    mysql: {
      // 单数据库配置信息
      client: {
        // host
        host: '39.108.229.103',
        // 端口号
        port: '3306',
        // 用户名
        user: 'root',
        // 密码
        password: 'lu1477212.',
        // 数据库名
        database: 'wudi',
      },
      // 是否加载到 app 上，默认开启
      app: true,
      // 是否加载到 agent 上，默认关闭
      agent: false,
    },
  }

  // cors
  config.security = {
    csrf: {
      enable: false,
      ignoreJSON: true,
    },
    domainWhiteList: ['*'],
  }
  // config.default.js
  config.cors = {
    origin: '*',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
  }

  // 添加 view 配置
  exports.view = {
    defaultViewEngine: 'nunjucks',
    mapping: {
      '.tpl': 'nunjucks',
    },
  }

  return {
    ...config,
    ...userConfig,
  }
}
