/*
 * @Author: LuZeng
 * @Date: 2022-07-29 17:25:14
 * @LastEditTime: 2022-08-16 10:29:06
 * @LastEditors: LuZeng
 * @Description: 小白本白，写的不好多多包涵！！！
 * @FilePath: \ThreeNode\app\config\plugin.js
 * 别乱动！
 */
"use strict";

/** @type Egg.EggPlugin */
module.exports = {
  // config/plugin.js
  nunjucks: {
    enable: true,
    package: "egg-view-nunjucks",
  },
  mysql: {
    enable: true,
    package: "egg-mysql",
  },
  // plugin.js
  cors: {
    enable: true,
    package: "egg-cors",
  },
  jwt: {
    enable: true,
    package: "egg-jwt",
  },
};
