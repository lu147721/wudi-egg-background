"use strict"; // 执行yarn test

const { app } = require("egg-mock/bootstrap");

describe("student test", () => {
  // 创建测试用例 名称 回调
  it("student index", () => {
    // 同步执行 一个一个执行
    return app
      .httpRequest()
      .get("/student") // 地址
      .expect(200) // 状态码
      .expect("学生信息"); // 期望的值
  });

  it("student lists", async () => {
    // 异步执行 全部执行
    await app
      .httpRequest()
      .get("/student/lists")
      .expect(200)
      .expect('[{"id":123}]');
  });
});
